using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Targil090920
{
   public class MyDictionaryIntToString
    {
        private List<int> m_keys;      // { 12,      32,      -100 , 45}

        private List<string> m_values; // {"orange","monkey","apple", "monkey"}

        public void Add(int key, string value)
        {
            if (m_keys.Contains(key))
            {
                throw new ArgumentException($"An item with the same key {key} has already been added.value sent {value}");
            }
            m_keys.Add(key);
            m_values.Add(value);
        }

        public bool TryAdd(int key, string value)
        {
            if (m_keys.Contains(key))
            {
                return false;
            }
            m_keys.Add(key);
            m_values.Add(value);
            return true;
        }
        public bool TryGet(int key, out string value)
        {
            if (m_keys.Contains(key))
            {
                int index_of_key = m_keys.IndexOf(key);
                value = m_values [index_of_key];
                return true;
            }
            value = null;
            return false;
        }

        public string Get(int key)
        {
            if (m_keys.Contains(key))
            {
                int index_of_key = m_keys.IndexOf(key);
                string value = m_values[index_of_key];
                return value;
            }
            throw new ArgumentException($"cannot get item with key {key}. cannot find key");
        }

        public void AddOrOverwrite(int key, string value)
        {
            if (m_keys.Contains(key))
            {
                // overwrite
                int index_of_key = m_keys.IndexOf(key);
                m_values[index_of_key] = value;
            }
            else
            {
                // add
                m_keys.Add(key);
                m_values.Add(value);
            }

        }

        public void Clear()
        {
            // implement
            m_keys.Clear();
            m_values.Clear();
        }

        public bool ContainsKey(int key)
        {
            //return m_keys.Contains(key);

            //bool result = m_keys.Contains(key);
            //return result;

            if (m_keys.Contains(key))
                return true;
            else
                return false;
        }
        public bool ContainsValue(string value)
        {
            if (m_values.Contains(value))
                return true;
            else
                return false;
        }

        public int CountItemsWithThisValue(string value)
        {
            // int counter = 0;
            // m_values.ForEach(one_value =>
            //{
            //    if (one_value == value)
            //        counter = counter + 1;
            //});

            //return m_values.Where(one_value => one_value == value).Count();

            //int counter = m_values.Where(one_value => one_value == value).Count();
            //return counter;

            //List<string> new_list = new List<string>();
            //foreach (string one_value in m_values)
            //{
            //    if (one_value == value)
            //        new_list.Add(value);
            //}
            //return new_list.Count;

            int counter = 0;
            foreach (string one_value in m_values)
            {
                if (one_value == value)
                    counter = counter + 1;
            }
            return counter;
        }

        public List<int> GetKeys()
        {
            // implement
            // *etgar - return replica of the m_keys
            //return new List<int>(m_keys);
            return m_keys;
        }

        public List<string> GetValue()
        {
            // implement
            // *etgar - return replica of the m_values
            return m_values;
        }

        public bool TryRemove(int key)
        {
            if (m_keys.Contains(key))
            {
                int index_of_key = m_keys.IndexOf(key);
                m_keys.RemoveAt(index_of_key);
                m_values.RemoveAt(index_of_key);
                return true;
            }
            return false;
        }

        private static MyDictionaryIntToString CloneMyDictionaryIntToString(MyDictionaryIntToString me)
        {
            MyDictionaryIntToString result = new MyDictionaryIntToString();

            for (int i = 0; i < me.m_keys.Count; i++)
            {
                result.Add(me.m_keys[i], me.m_values[i]);
            }
            return result;
        }

        public static MyDictionaryIntToString operator +(MyDictionaryIntToString me, KeyValuePair<int, string> item)
        {

            // d1 = d2 + <key,value>
            // advanced

            // copy (clone) the dictionary into a new one -- result
            MyDictionaryIntToString result = new MyDictionaryIntToString();
            // wrong
            //result.m_keys = me.m_keys; 
            //result.m_values = me.m_values;

            // 1
            result = CloneMyDictionaryIntToString(me);

            // 2
            //result.Clear();
            //me.GetKeys().ForEach (one_key => {result.Add(one_key, me.Get(one_key)));

            result.TryAdd(item.Key, item.Value);
            return result;
        }
        public static MyDictionaryIntToString operator -(MyDictionaryIntToString me, int key)
        {
            MyDictionaryIntToString result = CloneMyDictionaryIntToString(me);
            result.TryRemove(key);
            return result;
        }
        public static MyDictionaryIntToString operator +(MyDictionaryIntToString mydic1, MyDictionaryIntToString mydic2)
        {
            // implement -- merge
            // hint, use this: (add new item to me)
            //item.Key
            //item.Value
            MyDictionaryIntToString result = CloneMyDictionaryIntToString(mydic1);
            mydic2.GetKeys().ForEach(one_key =>
            {
                if (result.ContainsKey(one_key))
                {
                    // 5
                    // result 5 -- orange
                    // mydic2 5 -- tuna
                    string one_string_contain_both_values = result.Get(one_key) + " " + mydic2.Get(one_key);
                    result.AddOrOverwrite(one_key, one_string_contain_both_values);
                }
                else
                    result.Add(one_key, mydic2.Get(one_key));
            }
            );
            return result;
        }

        public static MyDictionaryIntToString operator -(MyDictionaryIntToString mydic1, MyDictionaryIntToString mydic2)
        {
            // implement -- remove items with same keys
            // hint, use this: (add new item to me)
            //item.Key
            //item.Value
            return null;
        }

        public override string ToString()
        {
            // the return value will look like this for exmaple: 
            // { 1, "danny" } , {2, "moshe" } ...
            // implement
            return base.ToString();
        }

        public static bool operator ==(MyDictionaryIntToString mydic1, MyDictionaryIntToString mydic2)
        {
            // implement 
            // check if all keys are the same
            // check if all keys-values are the same
            // if so return true otherwise false
            return false;
        }
        public static bool operator !=(MyDictionaryIntToString mydic1, MyDictionaryIntToString mydic2)
        {
            return !(mydic1 == mydic2);
        }

        public static bool operator >(MyDictionaryIntToString mydic1, MyDictionaryIntToString mydic2)
        {
            // will return true if numebr of items in mydic1 is bigger than number of items in mydic2
            // implement 
            return false;
        }
        public static bool operator <(MyDictionaryIntToString mydic1, MyDictionaryIntToString mydic2)
        {
            // will return true if numebr of items in mydic1 is smaller than number of items in mydic2
            // implement 
            return false;
        }
        public override bool Equals(object obj)
        {
            // implement
            return false;
        }

    }
}
