﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _060920
{
    class MyDictionaryIntToString
    {
        private List<int> m_keys;      // { 12,      32,      -100 , 45}

        private List<string> m_values; // {"orange","monkey","apple", "monkey"}

        public void Add(int key, string value)
        {
            // implement
            // -- may crash. if key exist then crash. 
            // crash
            //throw new Exception();
            //throw new Exception($"key {key} aleady exists");
            //throw new ArgumentException("An item with the same key has already been added.");
        }

        public bool TryAdd(int key, string value)
        {
            // implement
            return false; // failed to add
        }
        public bool TryGet(int key, out string value)
        {
            value = null;

            // implement
            return false; // failed to find
        }

        public string Get(int key)
        {
            // -- may crash. if key does NOT exist in our dictionary. 
            // crash
            //throw new Exception("key not found");
            // implement
            return null;
        }

        public void AddOrOverwrite(int key, string value)
        {
            // implement

            // if not exist - add, if exist - overwrite
        }

        public void Clear()
        {
            // implement

        }

        public bool ContainsKey(int key)
        {
            // implement
            return false;
        }
        public bool ContainsValue(string value)
        {
            // implement
            return false;
        }

        public int CountItemsWithThisValue(string value)
        {
            // implement
            return 0;
        }

        public List<string> GetKeys()
        {
            // implement
            // *etgar - return replica of the m_keys
            return null;
        }

        public List<string> GetValue()
        {
            // implement
            // *etgar - return replica of the m_values
            return null;
        }

        public static MyDictionaryIntToString operator + (MyDictionaryIntToString me, KeyValuePair<int, string> item)
        {
            // implement
            // hint, use this: (add new item to me)
            //item.Key
            //item.Value
            return me;
        }
        public static MyDictionaryIntToString operator -(MyDictionaryIntToString me, int key)
        {
            // implement
            // hint, use this: (remove this key from me)
            //item.Key
            return me;
        }
        public static MyDictionaryIntToString operator +(MyDictionaryIntToString mydic1, MyDictionaryIntToString mydic2)
        {
            // implement -- merge
            // hint, use this: (add new item to me)
            //item.Key
            //item.Value
            return null;
        }

        public static MyDictionaryIntToString operator -(MyDictionaryIntToString mydic1, MyDictionaryIntToString mydic2)
        {
            // implement -- remove items with same keys
            // hint, use this: (add new item to me)
            //item.Key
            //item.Value
            return null;
        }

        public override string ToString()
        {
            // the return value will look like this for exmaple: 
            // { 1, "danny" } , {2, "moshe" } ...
            // implement
            return base.ToString();
        }

        public static bool operator ==(MyDictionaryIntToString mydic1, MyDictionaryIntToString mydic2)
        {
            // implement 
            // check if all keys are the same
            // check if all keys-values are the same
            // if so return true otherwise false
            return false;
        }
        public static bool operator !=(MyDictionaryIntToString mydic1, MyDictionaryIntToString mydic2)
        {
            return !(mydic1 == mydic2);
        }

        public static bool operator >(MyDictionaryIntToString mydic1, MyDictionaryIntToString mydic2)
        {
            // will return true if numebr of items in mydic1 is bigger than number of items in mydic2
            // implement 
            return false;
        }
        public static bool operator <(MyDictionaryIntToString mydic1, MyDictionaryIntToString mydic2)
        {
            // will return true if numebr of items in mydic1 is smaller than number of items in mydic2
            // implement 
            return false;
        }
        public override bool Equals(object obj)
        {
            // implement
            return false;
        }

    }
}
